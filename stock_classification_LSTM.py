# -*- coding: utf-8 -*-
"""
Created on Fri May 24 14:55:17 2019

@author: Caio
"""

import pandas as pd
import numpy as np
import matplotlib.pyplot as plt
from keras.models import Sequential
from keras.layers import Dense, CuDNNLSTM
from sklearn.preprocessing import MinMaxScaler
from sklearn.model_selection import train_test_split 

#Training Parameters
#delay is the number of days taken in account at our regression
delay = 7
batch_size = 120
sample_size = 3500
number_of_epochs = 200
data_path = ""
train_cols = ["high","low","open","close"]

#Transforming the data into supervisoned format
def build_timeseries(mat, y_col_index):
    # y col is the index of the column you want to predict
    # total number of time-series samples would be len(mat) - delay
    dim_0 = mat.shape[0] - delay
    dim_1 = mat.shape[1]
    x = np.zeros((dim_0, delay, dim_1))
    y = np.zeros((dim_0))
    
    for i in range(0, dim_0):
        x[i] = mat[i:delay+i]
        y[i] = mat[delay+i, y_col_index]
    return x, y

#Trims dataset to a size that's divisible by BATCH_SIZE
def trim_dataset(mat):
    no_of_rows_drop = mat.shape[0]%batch_size
    if no_of_rows_drop > 0:
        return mat[:-no_of_rows_drop]
    else:
        return mat


#Loading the stock market data, reversing it and taking the most recent records
market_data = pd.read_csv(data_path).iloc[::-1]
market_data = market_data.tail(sample_size)

#Splitting the data into train and test set
market_data_train, market_data_test = train_test_split(market_data, train_size=0.9, test_size=0.1, shuffle=False)

# scale the feature MinMax, build array
x = market_data_train.loc[:,train_cols].values
min_max_scaler = MinMaxScaler()
market_data_train = min_max_scaler.fit_transform(x)
market_data_test = min_max_scaler.transform(market_data_test.loc[:,train_cols])

#Converting the data into supervisoned format
train_x, train_y = build_timeseries(market_data_train, 3)
test_x, test_y = build_timeseries(market_data_test, 3)

#trimming the matrixes to match our batch size
train_x = trim_dataset(train_x)
train_y = trim_dataset(train_y)
test_x = trim_dataset(test_x)
test_y = trim_dataset(test_y)

#Defining the LSTM model
lstm_model = Sequential()
#If you don't use CUDNN you can replace the CUDNNLSTM layer for the standard LSTM layer
lstm_model.add(CuDNNLSTM(100, batch_input_shape=(batch_size, delay, train_x.shape[2]), stateful=True))
lstm_model.add(Dense(1))
lstm_model.compile(loss='mean_squared_error', optimizer='adam')

#Fitting the model
lstm_model.fit(train_x, train_y, epochs= number_of_epochs, batch_size= batch_size, verbose=0)

#Making predictions for the test set
predict = lstm_model.predict(test_x, batch_size= batch_size)

#Printing the MSE
mse = ((test_y - predict)**2).mean(axis=None)
print("MSE = " + str(mse))

#Plotting the graph with Matplotlib
plt.plot(predict)
plt.plot(test_y)
plt.show()

